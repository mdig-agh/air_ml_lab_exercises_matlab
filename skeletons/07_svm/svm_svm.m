%
%   TOPIC: Support Vector Machines
%
% ------------------------------------------------------------------------

close all
clearvars

%% Generate data.

rng(1); % For reproducibility

n_cls = 100; % Number of samples in each class.

r = sqrt(rand(n_cls,1)); % Radius
t = 2 * pi * rand(n_cls,1);  % Angle
X_cls1 = [r .* cos(t), r .* sin(t)]; % Points

r2 = sqrt(3 * rand(n_cls,1) + 1); % Radius
t2 = 2 * pi * rand(n_cls,1);      % Angle
X_cls2 = [r2 .* cos(t2), r2 .* sin(t2)]; % points

X = vertcat(X_cls1, X_cls2);
Y = vertcat(-1 * ones(n_cls,1), +1 * ones(n_cls,1));

%% Fit a model.

% FIXME: implement

%% Visualize data and the model.

% FIXME: implement
