%
%   TOPIC: Support Vector Classifiers
%
% ------------------------------------------------------------------------

close all
clearvars

%% Generate data.

rng(1); % For reproducibility

% Generate data from a normal distribution.
n_cls = 20; % Number of samples in each class.
X = vertcat(...
    horzcat(normrnd(0.5,1, n_cls,1), normrnd(0.4,1, n_cls,1)), ...
    horzcat(normrnd(-0.3,1, n_cls,1), normrnd(-0.5,1, n_cls,1)) ...
    );
Y = vertcat(-1 * ones(n_cls,1), +1 * ones(n_cls,1));

%% Fit a model.

% FIXME: implement

%% Make predictions.

newX = [
    1, -0.4;
    1, -0.85
    ];

% FIXME: implement

%% Visualize data and the model.

% FIXME: implement

%% Get posteriors.

% FIXME: implement
