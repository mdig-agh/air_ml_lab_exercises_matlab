%% Uczenie maszynowe AiR, 2018
%%
%% Cwiczenie: K-najblizszych sasiadow
% Cel: Wykorzystanie wbudowanych funkcji do klasyfikacji danych przy pomocy
% algorytmu k-nn

% Prosze uzupelnic brakujace fragmenty zgodnie z instrukcj? (FIXME)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% K-nearest 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Prosze pobrac dane
clear all
%Fisher's 1936 iris data
load fisheriris.mat

%lub
% Cardiac arrhythmia data from the UCI machine learning repository:
%load arrhythmia.mat

% Prosze zapoznac sie ze zbiorem danych -> zmienna Description w Workspace

%Prosze uzywac funkcji fitcknn(), funkcja knnclassify() zostanie w
%przyszlosci ca?lkowicie wycofana

%Zadania

% Zaprojektuj klasyfikator typu k najblizszych sasiadow (k-NN) do
% rozpoznawania kwiatow irysa lub rodzaju arytmii.

% Zadanie 1

% Podziel zbior danych na uczacy i testowy. Losowo wybierz 5 danych do
%zbioru testowego

%FIXME

% Zadanie 2
% Narysuj dane uczace oraz testowe

%FIXME

% Zadanie 3 
% Znajdz 5 punktow najblizszych punktowi badanemu (pierwszy ze zbioru testowego)
% Skorzystaj z funkcji knnsearch()

%FIXME

% Narysuj te punkty na wykresie

%FIXME 

% Zadanie 4
%Ustal gatunki sasiadow. Skorzystaj z funckji tabulate()

%FIXME


% Zadanie 5
% Wykorzystujac funkcj? fitcknn() stworz klasyfikator dla k=4

%FIXME


%Zadanie 6 
%Sklasyfikuj dane ze zbioru testowego, funkcja predict()

%FIXME

%Zadanie 7
% W procedurze 10-krotnej walidacji krzyzowej znajdz optymalna wartosc liczby najblizszych sasiadow k:
%Przydatne funckje: crossvalind(), fitcknn()
%Dokladnosc klasyfikatora: ACC

%FIXME

%Narysuj wykres zaleznosci dokladnosci klasyfikatora (ACC) od wartosci k.

%FIXME

%Wybierz optymalna wartosc k

%FIXME

%Zadanie 8
% Przedstaw na wykresie granice klas

%Stworz klasyfikator kNN dla 2 wybranych parametrow:

%FIXME

%Dane testowe- przestrzen (X- parametry)(odkomentuj:)
%x1_range = min(X(:,1)):.01:max(X(:,1));
%x2_range = min(X(:,2)):.01:max(X(:,2));
%[xx1, xx2] = meshgrid(x1_range,x2_range);
%XGrid = [xx1(:) xx2(:)];

% Sklasyfikuj dane ze zbioru testowego

%FIXME

% Narysuj wykres (gscatter())

%FIXME

% DODATKOWO
%Prosze zapoznac sie z parametrami funkcji fitcknn() : metryki
%odleglosci(distance metrics), wagi (Distance Weights) ect.



